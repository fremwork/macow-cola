package com.macow.springcloud.api;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.Response;
import com.macow.springcloud.dto.CustomerAddCmd;
import com.macow.springcloud.dto.CustomerListByNameQry;
import com.macow.springcloud.dto.data.CustomerDTO;

public interface CustomerServiceI {

    public Response addCustomer(CustomerAddCmd customerAddCmd);

    public MultiResponse<CustomerDTO> listByName(CustomerListByNameQry customerListByNameQry);
}
