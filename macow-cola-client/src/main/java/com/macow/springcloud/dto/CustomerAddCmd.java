package com.macow.springcloud.dto;

import com.macow.springcloud.dto.data.CustomerDTO;
import lombok.Data;

@Data
public class CustomerAddCmd{

    private CustomerDTO customerDTO;

}
