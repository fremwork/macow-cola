package com.macow.springcloud.domain.customer.gateway;

import com.macow.springcloud.domain.customer.Customer;

public interface CustomerGateway {
    public Customer getByById(String customerId);
}
